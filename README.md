# Getting Started with Mifa Abiyyu CRM

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) And [Laravel](https://laravel.com)

## Available Scripts

In the project directory, you can run:

### `1. Clone Repository in branch Main and Backend`

### `2. Run npm install in Main Branch`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `3. npm start in Main Branch to Run Front End`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `4. Run composer install in Backend Branch`

### `5. Run cp .env.example .env.`

### `6. Run php artisan key:generate.`

### `7. Run php artisan migrate.`

### `8. Run php artisan migrate --seed .`

### `9. Run php artisan serve .`

**Note: Dont forget change url API in Main Branch**

Location in API.jsx

### User :

email: admin@admin.com
password : password

### User Roles and Permission :

Roles Admin :

1. CRUD Products
2. CRUD Category
3. Approve User
4. CRUD User
5. CRUD Customer
6. Approve Customer Order
7. CRUD Coupon

Roles Customer

1. Login
2. Buy Product
3. Cancel Order
4. Get Discount Product
5. Get Coupon Product
