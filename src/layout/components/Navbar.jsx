import axios from "axios";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { apiUrl } from "../../setup/Api";

export default function Navbar() {
  const navigate = useNavigate();

  const logoutUser = async () => {
    try {
      const response = await axios.post(
        apiUrl + "/api/auth/logout",
        {},
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );
      if (response.status === 200) {
        navigate("/");
        localStorage.removeItem("Token");
        localStorage.removeItem("User");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <nav className='fixed z-30 w-full bg-white border-b-2 border-indigo-600'>
      <div className='px-6 py-3'>
        <div className='flex items-center justify-between'>
          <div className='flex items-center justify-start'>
            <button className='p-2 text-gray-600 rounded cursor-pointer lg:hidden '>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='w-6 h-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth='2'
                  d='M4 6h16M4 12h16M4 18h16'
                />
              </svg>
            </button>
            <Link to='/' className='flex items-center text-xl font-bold'>
              <span className='text-blue-800'>Logo</span>
            </Link>
          </div>
          <div className='flex items-center'>
            <h2 className='text-blue-500 pr-1'>Hello, </h2>
            <div className='relative inline-block '>
              <h2 className='text-blue-500'> Admin </h2>
            </div>
            <button
              onClick={logoutUser}
              className='w-full px-4 py-2 bg-red-200 hover:bg-red-500 hover:text-white rounded mx-3'>
              Sign out
            </button>
          </div>
        </div>
      </div>
    </nav>
  );
}
