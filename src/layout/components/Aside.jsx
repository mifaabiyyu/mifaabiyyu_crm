import React from "react";
import { Link } from "react-router-dom";

export default function Aside() {
  return (
    <div className='flex flex-col w-full px-4 py-8 overflow-y-auto border-b lg:border-r lg:h-screen lg:w-64 shadow-lg'>
      <div className='flex flex-col justify-between mt-6'>
        <aside>
          <ul>
            <li>
              <Link
                className='flex items-center px-4 py-2 text-gray-700 bg-gray-100 rounded-md '
                to='/dashboard'>
                <i className='fa fa-home' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Dashboard</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/user'>
                <i className='fa fa-users' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Users</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/customer'>
                <i className='fa fa-user' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Customer</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/product'>
                <i className='fa fa-shopping-cart' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Products</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/customer-order'>
                <i className='fa fa-shopping-cart' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Customer Order</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/category'>
                <i className='fa fa-envelope-open' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Category</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/coupon'>
                <i className='fa fa-shopping-basket' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Coupon</span>
              </Link>
            </li>
            <li>
              <Link
                className='flex items-center px-4 py-2 mt-5 text-gray-600 rounded-md hover:bg-gray-200'
                to='/message'>
                <i className='fa fa-envelope' aria-hidden='true'></i>

                <span className='mx-4 font-medium'>Message</span>
              </Link>
            </li>
          </ul>
        </aside>
      </div>
    </div>
  );
}
