import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { apiUrl } from "../../setup/Api";
import { CheckAuthCustomer } from "../../setup/AuthCheck";
import Header from "../components/Header";

export default function CheckOut() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [product, setProduct] = useState("");
  const [coupon, setCoupon] = useState([]);
  const [price, setPrice] = useState("");
  const [message, setMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const { slug } = useParams();

  const user = localStorage.getItem("User");

  const userData = JSON.parse(user);

  const onSubmit = async (e) => {
    e.preventDefault();
    setMessage("");
    setErrorMessage("");
    try {
      setLoading(true);
      const response = await axios.post(
        apiUrl + "/api/customer_order/store",
        {
          user_id: userData.id,
          product_id: product.id,
          price: price,
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setLoading(false);
      setMessage(response.data.message);
      setTimeout(() => {
        navigate("/");
      }, 2000);
    } catch (error) {
      setErrorMessage(error.response.data.message);
      setLoading(false);
    }
  };

  const checkAuth = () => {
    const check = CheckAuthCustomer();

    if (check === false) {
      navigate("/login");
    }
  };

  const getProduct = async () => {
    const response = await axios.get(
      apiUrl + "/api/product/show_product/" + slug,
      {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      }
    );

    const resData = response.data.data;

    if (product.sale_price) {
      setPrice(resData.sale_price);
    } else {
      setPrice(resData.price);
    }

    setProduct(resData);

    const responseCoupon = await axios.get(
      apiUrl + "/api/coupon/get_by_product/" + resData.id,
      {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      }
    );

    const resDataCoupon = responseCoupon.data.data;
    setCoupon(resDataCoupon);
  };

  const setCouponProduct = (id) => {
    if (product.sale_price) {
      setPrice(product.sale_price);
    } else {
      setPrice(product.price);
    }

    const find = coupon.filter((data) => {
      return data.id == id; // eslint-disable-line
    });

    // console.log(find);
    if (find[0].discount) {
      if (product.sale_price) {
        let count = product.sale_price - find[0].discount;
        setPrice(count);
      } else {
        let count = product.price - find[0].discount;
        setPrice(count);
      }
    }

    if (find[0].percent) {
      if (product.sale_price) {
        let count = (find[0].percent / 100) * product.sale_price;
        let min = product.sale_price - count;
        setPrice(min.toFixed(0));
      } else {
        let count = (find[0].percent / 100) * product.price;
        let min = product.price - count;
        setPrice(min.toFixed(0));
      }
    }
  };

  useEffect(() => {
    checkAuth();
    getProduct();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <Header />
      <form
        id='whoobe-t7qyk'
        onSubmit={onSubmit}
        className='justify-center items-center w-full shadow-lg rounded-lg bg-white px-6 mt-20 flex flex-col md:w-1/2 lg:w-1/3 m-auto'>
        <h2 className='text-2xl my-4 font font-semibold'>Check Out Product</h2>

        <div
          id='whoobe-h90kl'
          className='w-full p-2 justify-start flex flex-col'>
          {message && (
            <div
              className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
              role='alert'>
              <svg
                className='inline flex-shrink-0 mr-3 w-5 h-5'
                fill='currentColor'
                viewBox='0 0 20 20'
                xmlns='http://www.w3.org/2000/svg'>
                <path
                  fill-rule='evenodd'
                  d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                  clip-rule='evenodd'></path>
              </svg>
              <div>
                <span className='font-medium'>Success!</span> {message}
              </div>
            </div>
          )}
          {errorMessage && (
            <div
              className='flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800'
              role='alert'>
              <svg
                className='inline flex-shrink-0 mr-3 w-5 h-5'
                fill='currentColor'
                viewBox='0 0 20 20'
                xmlns='http://www.w3.org/2000/svg'>
                <path
                  fill-rule='evenodd'
                  d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                  clip-rule='evenodd'></path>
              </svg>
              <div>
                <span className='font-medium'>Error!</span> {errorMessage}
              </div>
            </div>
          )}
          <div className='flex text-center justify-center items-center mb-10'>
            <img
              src={apiUrl + "/img/products/" + product.image}
              className='w-72 '
              alt=''
            />
          </div>
          {product.sale_price && (
            <div className='flex text-center justify-center items-center mb-5'>
              <h1 className='text-green-500'>
                Selamat !! Anda mendapatkan Harga Diskon
              </h1>
            </div>
          )}

          <div className='text-center flex justify-center items-center mb-5'>
            <select
              id='user'
              className='bg-gray-50 w-60 text-center border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5'
              onChange={(e) => setCouponProduct(e.target.value)}>
              <option selected disabled>
                -- Select Coupon --
              </option>
              <option value=''>No Coupon</option>
              {coupon.map((val, i) => {
                return (
                  <option key={val.id} value={val.id}>
                    {val.name}
                  </option>
                );
              })}
            </select>
          </div>

          {/* <p className='text-red-600'>{errormsg.email}</p> */}
          <table className='border-none '>
            <tbody>
              <tr className='h-12'>
                <td className=''>
                  <span
                    id='whoobe-4occ6'
                    className=' z-highest rounded-l-lg w-10 h-10 justify-center items-center text-2xl text-gray-400 border border-r-0'
                    mode='render'
                    block=''>
                    <i className='fa fa-shopping-bag' aria-hidden='true'></i>
                  </span>
                  <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                    Product
                  </h1>
                </td>
                <td className=''>
                  <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                    : {product.name}
                  </h1>
                </td>
              </tr>
              <tr className='h-12'>
                <td className=''>
                  <span
                    id='whoobe-4occ6'
                    className=' z-highest rounded-l-lg w-10 h-10 justify-center items-center text-2xl text-gray-400 border border-r-0'
                    mode='render'
                    block=''>
                    <i className='fa fa-shopping-bag' aria-hidden='true'></i>
                  </span>
                  <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                    Price
                  </h1>
                </td>
                <td className=''>
                  {product.sale_price && (
                    <div>
                      <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                        :
                      </h1>
                      <h1 className='inline ml-2 font-semibold mb-2 text-sm line-through text-gray-900 '>
                        Rp. {product.price}
                      </h1>
                      <h1 className='inline ml-2 font-semibold mb-2 text-xl text-red-600 '>
                        Rp. {product.sale_price}
                      </h1>
                    </div>
                  )}
                  {!product.sale_price && (
                    <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                      : Rp. {product.price}
                    </h1>
                  )}
                </td>
              </tr>
            </tbody>
          </table>
          <div id='whoobe-l6k6r' className='my-4 flex flex-row'>
            <div>
              <span
                id='whoobe-4occ6'
                className=' z-highest rounded-l-lg w-10 h-10 justify-center items-center text-2xl text-gray-400 border border-r-0'
                mode='render'
                block=''>
                <i className='fa fa-shopping-bag' aria-hidden='true'></i>
              </span>
              <h1 className='inline-block item-center justify-center mt-1 ml-3 mb-5 font-semibold text-xl'>
                Description
              </h1>
              <h1 className='text-justify mx-5'>{product.description}</h1>
            </div>
          </div>
          <table className='border-none '>
            <tbody>
              <tr className='h-12'>
                <td className=''>
                  <span
                    id='whoobe-4occ6'
                    className=' z-highest rounded-l-lg w-10 h-10 justify-center items-center text-2xl text-gray-400 border border-r-0'
                    mode='render'
                    block=''>
                    <i className='fa fa-shopping-bag' aria-hidden='true'></i>
                  </span>
                  <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                    Total
                  </h1>
                </td>
                <td className=''>
                  <h1 className='inline ml-2 font-semibold mb-2 text-xl text-gray-900 '>
                    : Rp. {price}
                  </h1>
                </td>
              </tr>
            </tbody>
          </table>
          {/* <p className='text-red-600'>{errormsg.password}</p> */}

          <button
            value='submit'
            className='px-4 py-2 rounded bg-blue-400 text-white hover:bg-blue-700 my-4 w-full'
            id='whoobe-ibemp'>
            {!loading && <p>Check out</p>}
            {loading && <p>Please wait ....</p>}
          </button>
        </div>
        <Link to='/' className='mb-10'>
          Back
        </Link>
      </form>
    </div>
  );
}
