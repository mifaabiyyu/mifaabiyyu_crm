import axios from "axios";
import React, { useEffect, useState } from "react";
import { apiUrl } from "../../setup/Api";
import Header from "../components/Header";
import Products from "../components/Products";

export default function ProductsAll() {
  const [products, setProducts] = useState([]);
  const [filterProducts, setFilterProducts] = useState([]);
  const [categoryData, setCategory] = useState([]);
  //   const [categoryId, setCategoryId] = useState("");

  const getCategory = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/category/all", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });
      setCategory(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const filterProduct = async (val) => {
    if (!val) {
      return getProduct();
    } else {
      const dataFilter = products;
      const productFilter = dataFilter.filter((product) => {
        return product.get_category.id == val; // eslint-disable-line
      });

      return setFilterProducts(productFilter);
    }
  };

  const getProduct = async () => {
    const response = await axios.get(apiUrl + "/api/product/all", {
      Accept: "application/json",
    });

    const resData = response.data.data;

    setProducts(resData);
    setFilterProducts(resData);
  };

  useEffect(() => {
    getProduct();
    getCategory();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <div>
      <Header />
      <div className='pt-20 pb-40 md:container md:mx-auto px-10 lg:px-20 md:px-20'>
        <div className='text-center pb-10'>
          <h1 className='font-bold text-3xl'>Our List Products</h1>
        </div>
        <div className='text-center flex justify-center items-center'>
          <select
            id='user'
            className='bg-gray-50 w-60 text-center border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5'
            onChange={(e) => filterProduct(e.target.value)}>
            <option selected disabled>
              -- Select Category --
            </option>
            <option value=''>All</option>
            {categoryData.map((val, i) => {
              return (
                <option key={val.id} value={val.id}>
                  {val.name}
                </option>
              );
            })}
          </select>
        </div>
        {filterProducts.map((data, i) => {
          return (
            <Products
              key={data.id}
              name={data.name}
              price={data.price}
              image={data.image}
              salePrice={data.sale_price}
              slug={data.slug}
            />
          );
        })}
        <div className='text-center mt-10'></div>
      </div>
    </div>
  );
}
