import React, { useEffect, useState } from "react";
import Header from "../components/Header";
// import header1 from "../../../public/img/header1.jpg";
import "flowbite";
import Slider from "../components/Slider";
import Products from "../components/Products";
import axios from "axios";
import { apiUrl } from "../../setup/Api";
import { Link } from "react-router-dom";
import { CheckAuthCustomer } from "../../setup/AuthCheck";

export default function Index() {
  const [auth, setAuth] = useState(false);
  const [products, setProducts] = useState([]);
  const [saleProducts, setSaleProducts] = useState([]);
  const [message, setMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const user = localStorage.getItem("User");

  const userData = JSON.parse(user);

  const getProduct = async () => {
    const response = await axios.get(apiUrl + "/api/product/all", {
      Accept: "application/json",
    });

    const resData = response.data.data;

    setProducts(resData);
  };

  const onSubmit = async (e) => {
    setLoading(true);
    setErrorMessage("");

    e.preventDefault();
    setSuccessMessage("");
    try {
      const response = await axios.post(
        apiUrl + "/api/message/store",
        {
          message: message,
          user_id: userData.id,
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setLoading(false);
      setMessage("");
      setSuccessMessage(response.data.message);
    } catch (error) {
      setLoading(false);
      setErrorMessage(error.response.data.errors.name);
    }
  };

  const getSaleProduct = async () => {
    const response = await axios.get(apiUrl + "/api/product/sale", {
      Accept: "application/json",
    });

    const resData = response.data.data;

    setSaleProducts(resData);
  };

  const checkAuth = () => {
    const check = CheckAuthCustomer();

    setAuth(check);
  };

  useEffect(() => {
    getProduct();
    getSaleProduct();
    checkAuth();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <Header />
      <Slider />
      <div className='pt-20 md:container md:mx-auto px-10 lg:px-20 md:px-20'>
        <div className='text-center pb-10'>
          <h1 className='font-bold text-3xl'>Sale Products</h1>
        </div>
        {saleProducts.map((data, i) => {
          return (
            <Products
              key={data.id}
              name={data.name}
              price={data.price}
              image={data.image}
              salePrice={data.sale_price}
              slug={data.slug}
            />
          );
        })}
      </div>

      <div className='pt-20 pb-40 md:container md:mx-auto px-10 lg:px-20 md:px-20'>
        <div className='text-center pb-10'>
          <h1 className='font-bold text-3xl'>Our List Products</h1>
        </div>
        {products.slice(0, 4).map((data, i) => {
          return (
            <Products
              key={data.id}
              name={data.name}
              price={data.price}
              image={data.image}
              salePrice={data.sale_price}
              slug={data.slug}
            />
          );
        })}
        <div className='text-center mt-10'>
          <Link
            to={"/all-product"}
            className='px-3 py-2 font-semibold text-white bg-blue-800 rounded hover:bg-blue-300 hover:text-black'>
            Show All Product
          </Link>
        </div>
      </div>
      {auth && (
        <div className='pb-40 md:container md:mx-auto px-10 lg:px-20 md:px-20'>
          <div className='text-center pb-10'>
            <h1 className='font-bold text-3xl'>Message Us</h1>
          </div>
          <div className='item-center justify-center text-center'>
            {errorMessage.message && (
              <div
                className='flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800'
                role='alert'>
                <svg
                  className='inline flex-shrink-0 mr-3 w-5 h-5'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                    clip-rule='evenodd'></path>
                </svg>
                <div>
                  <span className='font-medium'>Error !</span>{" "}
                  {errorMessage.message}
                </div>
              </div>
            )}
            {successMessage && (
              <div
                className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                role='alert'>
                <svg
                  className='inline flex-shrink-0 mr-3 w-5 h-5'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                    clip-rule='evenodd'></path>
                </svg>
                <div>
                  <span className='font-medium'>Success!</span> {successMessage}
                </div>
              </div>
            )}
            <form onSubmit={onSubmit}>
              <div className='mb-6'>
                <label
                  htmlFor='email'
                  className='block mb-2 text-sm font-medium text-gray-900 '>
                  Send Message to Admin
                </label>
                <div className='flex items-center justify-center text-center'>
                  <textarea
                    type='text'
                    id='message'
                    className='shadow-sm w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 '
                    placeholder='Enter Message'
                    onChange={(e) => {
                      setMessage(e.target.value);
                    }}
                    required
                  />
                </div>
              </div>
              <p className='text-red-600'>{errorMessage}</p>
              <button
                type='submit'
                className='text-white mt-3 bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
                {!loading && <p>Submit</p>}
                {loading && <p>Please wait ....</p>}
              </button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}
