import axios from "axios";
import React, { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useNavigate } from "react-router-dom";
import { apiUrl } from "../../setup/Api";
import { CheckAuthCustomer } from "../../setup/AuthCheck";
import Header from "../components/Header";

export default function CustomerOrder() {
  const [resData, setResData] = useState();
  const [message, setMessage] = useState();

  const user = localStorage.getItem("User");

  const userData = JSON.parse(user);

  const navigate = useNavigate();

  const confirmDelete = async (id) => {
    const confirmation = window.confirm("Are you sure delete?");

    if (confirmation) {
      const response = await axios.delete(
        apiUrl + "/api/customer_order/destroy/" + id,
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setMessage(response.data.message);
      getData();
    } else {
      return false;
    }
  };

  const getData = async () => {
    try {
      const response = await axios.get(
        apiUrl + "/api/customer_order/get_data_customer/" + userData.id,
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setResData(response.data.data.get_customer_order);
    } catch (error) {
      console.log(error);
    }
  };

  const checkAuth = () => {
    const check = CheckAuthCustomer();

    if (check === false) {
      navigate("/login");
    }
  };

  useEffect(() => {
    checkAuth();
    getData();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const columns = [
    {
      name: "Customer",
      style: "font-bold",
      selector: (row) => row.get_customer.name,
      sortable: true,
    },
    {
      name: "Product",
      selector: (row) => row.get_product.name,
      sortable: true,
    },
    {
      name: "Price",
      selector: (row) => row.price,
      sortable: true,
    },
    {
      name: "Order Date",
      cell: (row) => {
        var date = new Date(row.created_at);
        var newDate =
          date.getDate() +
          "-" +
          (date.getMonth() + 1) +
          "-" +
          date.getFullYear() +
          " " +
          date.getHours() +
          ":" +
          date.getMinutes() +
          ":" +
          date.getSeconds();

        return newDate;
      },
    },
    {
      name: "Status",
      cell: (row) => (
        <div>
          {row.status === 1 && (
            <h4 className='px-2 py-2 rounded bg-gray-600 font-bold text-white'>
              In Process
            </h4>
          )}
          {row.status === 2 && (
            <h4 className='px-2 py-2 rounded bg-green-600 font-bold text-white'>
              Approve
            </h4>
          )}
          {row.status === 3 && (
            <h4 className='px-2 py-2 rounded bg-red-600 font-bold text-white'>
              Reject
            </h4>
          )}
        </div>
      ),
      sortable: true,
    },
    {
      name: "Action",
      cell: (row) => (
        <div>
          {row.status === 1 && (
            <button
              className='px-2 py-2 bg-red-600 text-white hover:bg-red-300 hover:text-black rounded mr-1'
              onClick={() => confirmDelete(row.id)}>
              <i className='fa fa-trash' aria-hidden='true'></i>
            </button>
          )}
        </div>
      ),
    },
  ];

  const customStyles = {
    rows: {
      style: {
        minHeight: "72px",
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px", // override the cell padding for head cells
        paddingRight: "8px",
        fontWeight: "bold",
        fontSize: "150%",
      },
    },
    cells: {
      style: {
        paddingLeft: "8px", // override the cell padding for data cells
        paddingRight: "8px",
      },
    },
  };
  return (
    <div>
      <Header />
      <div className='pt-20 pb-40 md:container md:mx-auto px-10 lg:px-20 md:px-20'>
        <div className='text-center pb-10'>
          <h1 className='font-bold text-3xl'>List Order Customer</h1>
        </div>
        {message && (
          <div
            className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
            role='alert'>
            <svg
              className='inline flex-shrink-0 mr-3 w-5 h-5'
              fill='currentColor'
              viewBox='0 0 20 20'
              xmlns='http://www.w3.org/2000/svg'>
              <path
                fill-rule='evenodd'
                d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                clip-rule='evenodd'></path>
            </svg>
            <div>
              <span className='font-medium'>Success!</span> {message}
            </div>
          </div>
        )}
        <div className='text-center flex justify-center items-center'>
          <div className='border-4 border-b-cyan-700 h-full w-full shadow-lg'>
            <DataTable
              pagination
              columns={columns}
              data={resData}
              customStyles={customStyles}
            />
          </div>
        </div>

        <div className='text-center mt-10'></div>
      </div>
    </div>
  );
}
