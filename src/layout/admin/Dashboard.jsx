import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { CheckAuth } from "../../setup/AuthCheck";
// import { Link } from "react-router-dom";
import Aside from "../components/Aside";
import Navbar from "../components/Navbar";

export default function Dashboard() {
  const navigate = useNavigate();
  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
  });
  return (
    <div>
      <Navbar />
      <div className='pt-12 lg:flex'>
        <Aside />
        <div className='w-full h-full p-4 m-8 overflow-y-auto'>
          <div className='flex items-center justify-center p-16 mr-8 lg:p-40'>
            Dashboard
          </div>
        </div>
      </div>
    </div>
  );
}
