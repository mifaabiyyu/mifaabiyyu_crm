import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { apiUrl } from "../../../setup/Api";
import { CheckAuth } from "../../../setup/AuthCheck";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";

export default function EditUser() {
  const [loading, setLoading] = useState(false);
  const [errormsg, setErrorMsg] = useState({
    email: "",
    password: "",
    status: "",
    role: "",
    message: "",
  });
  const [message, setMessage] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [roles, setRoles] = useState("");

  const navigate = useNavigate();

  var { id } = useParams();

  const onSubmit = async (e) => {
    e.preventDefault();
    setErrorMsg({
      email: "",
      password: "",
      status: "",
      roles: "",
      message: "",
    });

    setLoading(true);
    try {
      const response = await axios.put(
        apiUrl + "/api/user/update/" + id,
        {
          email: email,
          password: password,
          password_confirmation: passwordConfirm,
          name: name,
          status: status,
          roles: roles,
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setMessage(response.data.message);
      setLoading(false);

      setTimeout(() => {
        navigate("/user");
      }, 2000);
    } catch (error) {
      setLoading(false);
      if (error.response.data.message) {
        setErrorMsg({
          message: error.response.data.message,
        });
      }

      setErrorMsg({
        message: error.response.data.message,
        email: error.response.data.errors.email,
        password: error.response.data.errors.password,
        name: error.response.data.errors.name,
        roles: error.response.data.errors.roles,
        status: error.response.data.errors.status,
      });
    }
  };

  const getData = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/user/show/" + id, {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      const resData = response.data.data;

      setEmail(resData.email);
      setName(resData.name);
      setStatus(resData.status);
      setRoles(resData.roles);
    } catch (error) {
      console.log(error);
    }
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getData();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <div>
        <Navbar />
        <div className='pt-12 lg:flex'>
          <Aside />
          <div className='w-full h-full p-4 m-8 overflow-y-auto '>
            <div className='items-center p-16 mr-8'>
              <div className='mb-5 text-right'>
                <Link
                  className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                  to={"/user"}>
                  Back
                </Link>
              </div>
              <div className=' h-full w-full'>
                <h1 className='font-bold text-2xl mb-10'>Form Edit User</h1>
                {errormsg.message && (
                  <div
                    className='flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800'
                    role='alert'>
                    <svg
                      className='inline flex-shrink-0 mr-3 w-5 h-5'
                      fill='currentColor'
                      viewBox='0 0 20 20'
                      xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fill-rule='evenodd'
                        d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                        clip-rule='evenodd'></path>
                    </svg>
                    <div>
                      <span className='font-medium'>Error !</span>{" "}
                      {errormsg.message}
                    </div>
                  </div>
                )}
                {message && (
                  <div
                    className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                    role='alert'>
                    <svg
                      className='inline flex-shrink-0 mr-3 w-5 h-5'
                      fill='currentColor'
                      viewBox='0 0 20 20'
                      xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fill-rule='evenodd'
                        d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                        clip-rule='evenodd'></path>
                    </svg>
                    <div>
                      <span className='font-medium'>Success!</span> {message}
                    </div>
                  </div>
                )}
                <form onSubmit={onSubmit}>
                  <div className='mb-6'>
                    <label
                      htmlFor='name'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Name
                    </label>
                    <input
                      type='text'
                      id='name'
                      value={name}
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Enter name'
                      onChange={(e) => setName(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600 mb-4'>{errormsg.name}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='email'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Email
                    </label>
                    <input
                      type='email'
                      id='email'
                      value={email}
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='name@mail.com'
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600 mb-4'>{errormsg.email}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Password
                    </label>
                    <input
                      type='password'
                      id='password'
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Password'
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </div>
                  <p className='text-red-600 mb-4'>{errormsg.password}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Password Confirmation
                    </label>
                    <input
                      type='password'
                      id='password_confirm'
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Password Confirm'
                      onChange={(e) => setPasswordConfirm(e.target.value)}
                    />
                  </div>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Status
                    </label>
                    <select
                      id='status'
                      value={status}
                      className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                      onChange={(e) => setStatus(e.target.value)}>
                      <option selected disabled>
                        -- Select Status --
                      </option>
                      <option value='0'>Not Active</option>
                      <option value='1'>Active</option>
                    </select>
                  </div>
                  <p className='text-red-600 mb-4'>{errormsg.status}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Role
                    </label>
                    <select
                      id='status'
                      value={roles}
                      className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                      onChange={(e) => setRoles(e.target.value)}>
                      <option selected disabled>
                        -- Select Role --
                      </option>
                      <option value='1'>Admin</option>
                      <option value='2'>Customer</option>
                    </select>
                  </div>
                  <p className='text-red-600 mb-4'>{errormsg.role}</p>
                  <button
                    type='submit'
                    className='text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
                    {!loading && <p>Submit</p>}
                    {loading && <p>Please wait ....</p>}
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
