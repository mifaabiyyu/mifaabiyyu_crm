import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { apiUrl } from "../../../setup/Api";
import { CheckAuth } from "../../../setup/AuthCheck";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";

export default function EditCustomer() {
  const [loading, setLoading] = useState(false);
  const [errormsg, setErrorMsg] = useState({
    email: "",
    name: "",
    address: "",
    phone: "",
    user_id: "",
    message: "",
  });
  const [message, setMessage] = useState("");

  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [userId, setUserId] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");

  const [userData, setDataUser] = useState([]);

  const navigate = useNavigate();
  var { id } = useParams();

  const onSubmit = async (e) => {
    e.preventDefault();
    setErrorMsg({
      email: "",
      name: "",
      address: "",
      phone: "",
      user_id: "",
      message: "",
    });

    setLoading(true);
    axios
      .put(
        apiUrl + "/api/customer/update/" + id,
        {
          email: email,
          name: name,
          address: address,
          phone: phone,
          user_id: userId,
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      )
      .then(function (response) {
        setMessage(response.data.message);

        setLoading(false);

        setTimeout(() => {
          navigate("/customer");
        }, 2000);
      })
      .catch(function (error) {
        setLoading(false);
        if (error.response.data.message) {
          setErrorMsg({
            message: error.response.data.message,
          });
        }

        setErrorMsg({
          message: error.response.data.message,
          email: error.response.data.errors.email,
          name: error.response.data.errors.name,
          address: error.response.data.errors.address,
          phone: error.response.data.errors.phone,
          user_id: error.response.data.errors.user_id,
        });
      });
  };

  const getUser = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/user/get_data", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });
      setDataUser(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getData = async () => {
    const response = await axios.get(apiUrl + "/api/customer/show/" + id, {
      headers: {
        Accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("Token"),
      },
    });

    const resData = response.data.data;

    setEmail(resData.email);
    setName(resData.name);
    setAddress(resData.address);
    setPhone(resData.phone);
    setUserId(resData.user_id);
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getUser();
    getData();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <div>
        <Navbar />
        <div className='pt-12 lg:flex'>
          <Aside />
          <div className='w-full h-full p-4 m-8 overflow-y-auto '>
            <div className='items-center p-16 mr-8'>
              <div className='mb-5 text-right'>
                <Link
                  className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                  to={"/customer"}>
                  Back
                </Link>
              </div>
              <div className=' h-full w-full'>
                <h1 className='font-bold text-2xl mb-10'>Form Edit Customer</h1>
                {errormsg.message && (
                  <div
                    className='flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800'
                    role='alert'>
                    <svg
                      className='inline flex-shrink-0 mr-3 w-5 h-5'
                      fill='currentColor'
                      viewBox='0 0 20 20'
                      xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fill-rule='evenodd'
                        d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                        clip-rule='evenodd'></path>
                    </svg>
                    <div>
                      <span className='font-medium'>Error !</span>{" "}
                      {errormsg.message}
                    </div>
                  </div>
                )}
                {message && (
                  <div
                    className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                    role='alert'>
                    <svg
                      className='inline flex-shrink-0 mr-3 w-5 h-5'
                      fill='currentColor'
                      viewBox='0 0 20 20'
                      xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fill-rule='evenodd'
                        d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                        clip-rule='evenodd'></path>
                    </svg>
                    <div>
                      <span className='font-medium'>Success!</span> {message}
                    </div>
                  </div>
                )}
                <form onSubmit={onSubmit}>
                  <div className='mb-6'>
                    <label
                      htmlFor='email'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Name
                    </label>
                    <input
                      type='text'
                      id='email'
                      value={name}
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Enter name'
                      onChange={(e) => setName(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.name}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='email'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Email
                    </label>
                    <input
                      type='email'
                      id='email'
                      value={email}
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='name@mail.com'
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.email}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Address
                    </label>
                    <input
                      type='text'
                      value={address}
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Enter address'
                      onChange={(e) => setAddress(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.address}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Phone
                    </label>
                    <input
                      type='number'
                      value={phone}
                      placeholder='Enter phone number'
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      onChange={(e) => setPhone(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.phone}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='countries'
                      className='block mb-2 text-sm font-medium text-gray-900'>
                      Select Users
                    </label>
                    {/* {user} */}
                    <select
                      id='user'
                      className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                      onChange={(e) => setUserId(e.target.value)}
                      value={userId}>
                      <option disabled>-- Select User --</option>
                      {userData.map((val, i) => {
                        return <option value={val.id}>{val.name}</option>;
                      })}
                    </select>
                  </div>
                  <p className='text-red-600 mb-5'>{errormsg.user_id}</p>
                  <button
                    type='submit'
                    className='text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
                    {!loading && <p>Submit</p>}
                    {loading && <p>Please wait ....</p>}
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
