import React, { useEffect, useState } from "react";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";
import DataTable from "react-data-table-component";
import axios from "axios";
import { apiUrl } from "../../../setup/Api";

import { Link, useNavigate } from "react-router-dom";
import { CheckAuth } from "../../../setup/AuthCheck";

export default function Coupon() {
  const [resData, setResData] = useState({
    coupon: [],
    message: "",
  });

  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const getCoupon = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/coupon/get_data", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      setResData({
        coupon: response.data.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const confirmDelete = async (id) => {
    const confirmation = window.confirm("Are you sure delete?");

    if (confirmation) {
      const response = await axios.delete(
        apiUrl + "/api/coupon/destroy/" + id,
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setMessage(response.data.message);
      getCoupon();
    } else {
      return false;
    }
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getCoupon();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const columnsCoupon = [
    {
      name: "Name",
      style: "font-bold",
      selector: (row) => row.name,
      sortable: true,
    },
    {
      name: "Discount (Rp. )",
      cell: (row) => (
        <div>
          <h1 className='font-semibold'>Rp. {row.discount}</h1>
        </div>
      ),
      sortable: true,
    },
    {
      name: "Discount (Percent)",
      selector: (row) => row.percent,
      sortable: true,
    },
    {
      name: "Product",
      selector: (row) => row.get_product.name,
      sortable: true,
    },
    {
      name: "Action",
      cell: (row) => (
        <div>
          <button
            className='px-2 py-2 bg-red-600 text-white hover:bg-red-300 hover:text-black rounded mr-1'
            onClick={() => confirmDelete(row.id)}>
            <i className='fa fa-trash' aria-hidden='true'></i>
          </button>
          <Link
            to={"/coupon/edit/" + row.id}
            className='px-2 py-2 bg-blue-600 text-white hover:bg-blue-300 hover:text-black rounded'>
            <i className='fa fa-pencil-square' aria-hidden='true'></i>
          </Link>
        </div>
      ),
    },
  ];

  const customStyles = {
    rows: {
      style: {
        minHeight: "72px",
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px", // override the cell padding for head cells
        paddingRight: "8px",
        fontWeight: "bold",
        fontSize: "150%",
      },
    },
    cells: {
      style: {
        paddingLeft: "8px", // override the cell padding for data cells
        paddingRight: "8px",
      },
    },
  };
  return (
    <div>
      <Navbar />
      <div className='pt-12 lg:flex'>
        <Aside />
        <div className='w-full h-full p-4 m-8 overflow-y-auto '>
          <h1 className='font-bold text-2xl mb-10 mt-10'>Menu Coupon</h1>
          <div className='items-center p-16 mr-8'>
            <div className='mb-5 text-right'>
              <Link
                to={"/coupon/add"}
                className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>
                Add Coupon
              </Link>
            </div>
            {message && (
              <div
                className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                role='alert'>
                <svg
                  className='inline flex-shrink-0 mr-3 w-5 h-5'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                    clip-rule='evenodd'></path>
                </svg>
                <div>
                  <span className='font-medium'>Success!</span> {message}
                </div>
              </div>
            )}
            <div className='border-4 border-b-cyan-700 h-full w-full shadow-lg'>
              <DataTable
                pagination
                columns={columnsCoupon}
                data={resData.coupon}
                customStyles={customStyles}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
