import React, { useEffect, useState } from "react";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";
import DataTable from "react-data-table-component";
import axios from "axios";
import { apiUrl } from "../../../setup/Api";

import { useNavigate } from "react-router-dom";
import { CheckAuth } from "../../../setup/AuthCheck";

export default function Category() {
  const [resData, setResData] = useState({
    category: [],
    message: "",
  });

  const [message, setMessage] = useState("");
  const [edit, setEdit] = useState("Add");
  const [name, setName] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [errormsg, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const getCategory = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/category/get_data", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      setResData({
        category: response.data.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const findCategory = async (id) => {
    setEdit("Edit");
    setCategoryId("");
    try {
      const response = await axios.get(apiUrl + "/api/category/show/" + id, {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      setName(response.data.data.name);
      setCategoryId(response.data.data.id);
    } catch (error) {
      console.log(error);
    }
  };

  const addForm = () => {
    setEdit("Add");
    setCategoryId("");
    setName("");
  };

  const onSubmit = async (e) => {
    setLoading(true);
    setErrorMessage("");

    e.preventDefault();
    setMessage("");
    try {
      const response = await axios.post(
        apiUrl + "/api/category/store",
        {
          name: name,
          id: categoryId,
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      getCategory();
      setLoading(false);
      setMessage(response.data.message);
      setCategoryId("");
    } catch (error) {
      setLoading(false);
      setErrorMessage(error.response.data.errors.name);
    }
  };

  const confirmDelete = async (id) => {
    setMessage("");
    const confirmation = window.confirm("Are you sure delete?");

    if (confirmation) {
      const response = await axios.delete(
        apiUrl + "/api/category/destroy/" + id,
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setMessage(response.data.message);
      getCategory();
    } else {
      return false;
    }
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getCategory();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const columnsCategory = [
    {
      name: "Name",
      style: "font-bold",
      selector: (row) => row.name,
      sortable: true,
    },
    {
      name: "Action",
      cell: (row) => (
        <div>
          <button
            className='px-2 py-2 bg-red-600 text-white hover:bg-red-300 hover:text-black rounded mr-1'
            onClick={() => confirmDelete(row.id)}>
            <i className='fa fa-trash' aria-hidden='true'></i>
          </button>
          <button
            onClick={() => findCategory(row.id)}
            className='px-2 py-2 bg-blue-600 text-white hover:bg-blue-300 hover:text-black rounded'>
            <i className='fa fa-pencil-square' aria-hidden='true'></i>
          </button>
        </div>
      ),
    },
  ];

  const customStyles = {
    rows: {
      style: {
        minHeight: "72px",
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px", // override the cell padding for head cells
        paddingRight: "8px",
        fontWeight: "bold",
        fontSize: "150%",
      },
    },
    cells: {
      style: {
        paddingLeft: "8px", // override the cell padding for data cells
        paddingRight: "8px",
      },
    },
  };
  return (
    <div>
      <Navbar />
      <div className='pt-12 lg:flex'>
        <Aside />
        <div className='w-full h-full p-4 m-8 overflow-y-auto '>
          <h1 className='font-bold text-2xl mb-10 mt-10'>Menu Category</h1>
          <div className='items-center p-16 mr-8'>
            <h1 className='font-bold text-xl mb-10'>Form {edit} Category</h1>
            <form onSubmit={onSubmit}>
              <div className='mb-6'>
                <label
                  htmlFor='email'
                  className='block mb-2 text-sm font-medium text-gray-900 '>
                  Name
                </label>
                <input
                  type='text'
                  id='email'
                  value={name}
                  className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                  placeholder='Enter name'
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                  required
                />
              </div>
              <p className='text-red-600'>{errormsg}</p>
              <button
                type='submit'
                className='text-white mt-3 bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
                {!loading && <p>Submit</p>}
                {loading && <p>Please wait ....</p>}
              </button>
            </form>
            <div className='mb-5 text-right'>
              <button
                onClick={addForm}
                className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>
                Add Category
              </button>
            </div>
            {message && (
              <div
                className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                role='alert'>
                <svg
                  className='inline flex-shrink-0 mr-3 w-5 h-5'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                    clip-rule='evenodd'></path>
                </svg>
                <div>
                  <span className='font-medium'>Success!</span> {message}
                </div>
              </div>
            )}
            <div className='border-4 border-b-cyan-700 h-full w-full shadow-lg'>
              <DataTable
                pagination
                columns={columnsCategory}
                data={resData.category}
                customStyles={customStyles}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
