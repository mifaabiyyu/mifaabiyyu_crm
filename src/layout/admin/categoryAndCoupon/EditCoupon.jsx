import React, { useEffect, useState } from "react";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";
import axios from "axios";
import { apiUrl } from "../../../setup/Api";

import { Link, useNavigate, useParams } from "react-router-dom";
import { CheckAuth } from "../../../setup/AuthCheck";

export default function EditCoupon() {
  const [resData, setResData] = useState({
    product: [],
    coupon: [],
    message: "",
  });

  const [message, setMessage] = useState("");
  const [name, setName] = useState("");
  const [discount, setDiscount] = useState("");
  const [percent, setPercent] = useState("");
  const [product, setProduct] = useState("");
  const [errormsg, setErrorMessage] = useState({
    name: "",
    discount: "",
    percent: "",
    product_id: "",
  });
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  var { id } = useParams();

  const getProduct = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/product/get_data", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      setResData({
        product: response.data.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const getCoupon = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/coupon/show/" + id, {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      const resData = response.data.data;

      setName(resData.name);
      setDiscount(resData.discount);
      setProduct(resData.product_id);
      setPercent(resData.percent);
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = async (e) => {
    setLoading(true);
    setErrorMessage("");

    e.preventDefault();
    setMessage("");
    try {
      const response = await axios.put(
        apiUrl + "/api/coupon/update/" + id,
        {
          name: name,
          discount: discount,
          percent: percent,
          product_id: product,
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setLoading(false);
      setMessage(response.data.message);

      setTimeout(() => {
        navigate("/coupon");
      }, 2000);
    } catch (error) {
      setLoading(false);
      setErrorMessage({
        name: error.response.data.errors.name,
        discount: error.response.data.errors.discount,
        product: error.response.data.errors.product_id,
        percent: error.response.data.errors.percent,
      });
    }
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getProduct();
    getCoupon();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <Navbar />
      <div className='pt-12 lg:flex'>
        <Aside />
        <div className='w-full h-full p-4 m-8 overflow-y-auto '>
          <div className='items-center p-16 mr-8'>
            <div className='mb-5 text-right'>
              <Link
                className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                to={"/coupon"}>
                Back
              </Link>
            </div>
            <h1 className='font-bold text-2xl mb-10 mt-10'>Form Edit Coupon</h1>
            {message && (
              <div
                className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                role='alert'>
                <svg
                  className='inline flex-shrink-0 mr-3 w-5 h-5'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                    clip-rule='evenodd'></path>
                </svg>
                <div>
                  <span className='font-medium'>Success!</span> {message}
                </div>
              </div>
            )}
            <form onSubmit={onSubmit}>
              <div className='mb-6'>
                <label
                  htmlFor='email'
                  className='block mb-2 text-sm font-medium text-gray-900 '>
                  Name
                </label>
                <input
                  type='text'
                  id='email'
                  value={name}
                  className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                  placeholder='Enter name'
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                  required
                />
              </div>
              <p className='text-red-600'>{errormsg.name}</p>
              <div className='mb-6'>
                <label
                  htmlFor='password'
                  className='block mb-2 text-sm font-medium text-gray-900 '>
                  Discount (Rp)
                </label>
                <div className='relative'>
                  <div className='absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none'>
                    Rp
                  </div>
                  <input
                    type='number'
                    value={discount}
                    className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  '
                    placeholder='Enter price'
                    onChange={(e) => setDiscount(e.target.value)}
                  />
                </div>
              </div>
              <p className='text-red-600'>{errormsg.discount}</p>
              <div className='mb-6'>
                <label
                  htmlFor='password'
                  className='block mb-2 text-sm font-medium text-gray-900 '>
                  Discount (Percent)
                </label>
                <div className='relative'>
                  <div className='absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none'>
                    %
                  </div>
                  <input
                    type='number'
                    value={percent}
                    className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  '
                    placeholder='Enter price'
                    onChange={(e) => setPercent(e.target.value)}
                  />
                </div>
              </div>
              <p className='text-red-600'>{errormsg.percent}</p>
              <div className='mb-6'>
                <label
                  htmlFor='countries'
                  className='block mb-2 text-sm font-medium text-gray-900'>
                  Select Product
                </label>
                {/* {user} */}
                <select
                  id='user'
                  className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                  onChange={(e) => setProduct(e.target.value)}
                  value={product}>
                  <option disabled>-- Select User --</option>
                  {resData.product.map((val, i) => {
                    return <option value={val.id}>{val.name}</option>;
                  })}
                </select>
              </div>
              <p className='text-red-600 mb-5'>{errormsg.product}</p>
              <button
                type='submit'
                className='text-white mt-3 bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
                {!loading && <p>Submit</p>}
                {loading && <p>Please wait ....</p>}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
