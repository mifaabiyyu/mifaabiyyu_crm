import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { apiUrl } from "../../../setup/Api";
import { CheckAuth } from "../../../setup/AuthCheck";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";

export default function EditProduct() {
  const [loading, setLoading] = useState(false);
  const [errormsg, setErrorMsg] = useState({
    image: "",
    name: "",
    price: "",
    phone: "",
    category_id: "",
    sale_price: "",
    description: "",
    message: "",
  });
  const [message, setMessage] = useState("");

  const [image, setImage] = useState("");
  const [oldImage, setOldImage] = useState("");
  const [name, setName] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [price, setPrice] = useState("");
  const [salePrice, setSalePrice] = useState("");
  const [slug, setSlug] = useState("");
  const [description, setDescription] = useState("");

  const [categoryData, setCategory] = useState([]);

  const navigate = useNavigate();

  var { id } = useParams();

  const onSubmit = async (e) => {
    e.preventDefault();
    setErrorMsg({
      image: "",
      name: "",
      price: "",
      slug: "",
      category_id: "",
      description: "",
      sale_price: "",
      message: "",
    });

    const data = new FormData();

    data.append("image", image);
    data.append("name", name);
    data.append("price", price);
    data.append("slug", slug);
    data.append("sale_price", salePrice);
    data.append("description", description);
    data.append("category_id", categoryId);

    setLoading(true);

    try {
      const response = await axios.post(
        apiUrl + "/api/product/update/" + id,
        data,
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      if (response) {
        setMessage(response.data.message);

        setLoading(false);

        setTimeout(() => {
          navigate("/product");
        }, 2000);
      }
    } catch (error) {
      setLoading(false);
      if (error.response.data.message) {
        setErrorMsg({
          message: error.response.data.message,
        });
      }

      setErrorMsg({
        message: error.response.data.message,
        slug: error.response.data.errors.slug,
        name: error.response.data.errors.name,
        price: error.response.data.errors.price,
        sale_price: error.response.data.errors.sale_price,
        description: error.response.data.errors.description,
        category_id: error.response.data.errors.category_id,
      });
    }
  };

  const getCategory = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/category/get_data", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });
      setCategory(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getProduct = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/product/show/" + id, {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      setCategoryId(response.data.data.category_id);
      setName(response.data.data.name);
      setPrice(response.data.data.price);
      setSalePrice(response.data.data.sale_price);
      setOldImage(response.data.data.image);
      setDescription(response.data.data.description);
      setSlug(response.data.data.slug);
    } catch (error) {
      console.log(error);
    }
  };

  const getSlug = (name) => {
    var slug = name
      .toLowerCase()
      .replace(/ /g, "-")
      .replace(/[^\w-]+/g, "");

    setSlug(slug);
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    getCategory();
    getProduct();
    checkAuth();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <div>
        <Navbar />
        <div className='pt-12 lg:flex'>
          <Aside />
          <div className='w-full h-full p-4 m-8 overflow-y-auto '>
            <div className='items-center p-16 mr-8'>
              <div className='mb-5 text-right'>
                <Link
                  className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
                  to={"/product"}>
                  Back
                </Link>
              </div>
              <div className=' h-full w-full'>
                <h1 className='font-bold text-2xl mb-10'>Form Edit Products</h1>
                {errormsg.message && (
                  <div
                    className='flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800'
                    role='alert'>
                    <svg
                      className='inline flex-shrink-0 mr-3 w-5 h-5'
                      fill='currentColor'
                      viewBox='0 0 20 20'
                      xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fill-rule='evenodd'
                        d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                        clip-rule='evenodd'></path>
                    </svg>
                    <div>
                      <span className='font-medium'>Error !</span>{" "}
                      {errormsg.message}
                    </div>
                  </div>
                )}
                {message && (
                  <div
                    className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                    role='alert'>
                    <svg
                      className='inline flex-shrink-0 mr-3 w-5 h-5'
                      fill='currentColor'
                      viewBox='0 0 20 20'
                      xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fill-rule='evenodd'
                        d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                        clip-rule='evenodd'></path>
                    </svg>
                    <div>
                      <span className='font-medium'>Success!</span> {message}
                    </div>
                  </div>
                )}
                <form onSubmit={onSubmit}>
                  <div className='mb-6'>
                    <label
                      htmlFor='email'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Name
                    </label>
                    <input
                      type='text'
                      id='email'
                      value={name}
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Enter name'
                      onChange={(e) => {
                        setName(e.target.value);
                        getSlug(e.target.value);
                      }}
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.name}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='email'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Slug
                    </label>
                    <input
                      type='text'
                      id='slug'
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      placeholder='Convert from name'
                      value={slug}
                      readOnly
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.email}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Price
                    </label>
                    <div className='relative'>
                      <div className='absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none'>
                        Rp
                      </div>
                      <input
                        type='number'
                        value={price}
                        className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  '
                        placeholder='Enter price'
                        onChange={(e) => setPrice(e.target.value)}
                        required
                      />
                    </div>
                  </div>
                  <p className='text-red-600'>{errormsg.price}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Sale Price
                    </label>
                    <div className='relative'>
                      <div className='absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none'>
                        Rp
                      </div>
                      <input
                        type='number'
                        value={salePrice}
                        className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  '
                        placeholder='Enter sale price'
                        onChange={(e) => setSalePrice(e.target.value)}
                      />
                    </div>
                  </div>
                  <p className='text-red-600'>{errormsg.price}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='countries'
                      className='block mb-2 text-sm font-medium text-gray-900'>
                      Category
                    </label>
                    {/* {user} */}
                    <select
                      id='user'
                      className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                      onChange={(e) => setCategoryId(e.target.value)}
                      value={categoryId}>
                      <option selected disabled>
                        -- Select Category --
                      </option>
                      {categoryData.map((val, i) => {
                        return <option value={val.id}>{val.name}</option>;
                      })}
                    </select>
                  </div>
                  <p className='text-red-600 mb-5'>{errormsg.category_id}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Image
                    </label>
                    <input
                      type='file'
                      placeholder='Enter phone number'
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      onChange={(e) => setImage(e.target.files[0])}
                    />

                    {image && (
                      <img
                        className='w-48 mt-5'
                        src={URL.createObjectURL(image)}
                        alt=''
                      />
                    )}
                    {oldImage && !image && (
                      <img
                        className='w-48 mt-5'
                        src={apiUrl + "/img/products/" + oldImage}
                        alt=''
                      />
                    )}
                  </div>
                  <p className='text-red-600'>{errormsg.image}</p>
                  <div className='mb-6'>
                    <label
                      htmlFor='password'
                      className='block mb-2 text-sm font-medium text-gray-900 '>
                      Description
                    </label>
                    <textarea
                      type='number'
                      placeholder='Enter phone number'
                      className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 '
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                      required
                    />
                  </div>
                  <p className='text-red-600'>{errormsg.phone}</p>
                  <button
                    type='submit'
                    className='text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
                    {!loading && <p>Submit</p>}
                    {loading && <p>Please wait ....</p>}
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
