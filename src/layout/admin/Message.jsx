import React, { useEffect, useState } from "react";
import Aside from "../components/Aside";
import Navbar from "../components/Navbar";
import DataTable from "react-data-table-component";
import axios from "axios";
import { apiUrl } from "../../setup/Api";

import { useNavigate } from "react-router-dom";
import { CheckAuth } from "../../setup/AuthCheck";

export default function Message() {
  const [resData, setResData] = useState({
    message: [],
  });

  const navigate = useNavigate();

  const getMessage = async () => {
    try {
      const response = await axios.get(apiUrl + "/api/message/get_data", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
      });

      setResData({
        message: response.data.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getMessage();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const columnsCategory = [
    {
      name: "Customer",
      style: "font-bold",
      selector: (row) => row.get_customer.name,
      sortable: true,
    },
    {
      name: "Message",
      style: "font-bold",
      selector: (row) => row.message,
      sortable: true,
    },
  ];

  const customStyles = {
    rows: {
      style: {
        minHeight: "72px",
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px", // override the cell padding for head cells
        paddingRight: "8px",
        fontWeight: "bold",
        fontSize: "150%",
      },
    },
    cells: {
      style: {
        paddingLeft: "8px", // override the cell padding for data cells
        paddingRight: "8px",
      },
    },
  };
  return (
    <div>
      <Navbar />
      <div className='pt-12 lg:flex'>
        <Aside />
        <div className='w-full h-full p-4 m-8 overflow-y-auto '>
          <h1 className='font-bold text-2xl mb-10 mt-10'>Menu Message</h1>
          <div className='items-center p-16 mr-8'>
            <div className='border-4 border-b-cyan-700 h-full w-full shadow-lg'>
              <DataTable
                pagination
                columns={columnsCategory}
                data={resData.message}
                customStyles={customStyles}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
