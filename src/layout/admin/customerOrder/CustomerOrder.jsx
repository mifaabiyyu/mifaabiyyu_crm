import React, { useEffect, useState } from "react";
import Aside from "../../components/Aside";
import Navbar from "../../components/Navbar";
import DataTable from "react-data-table-component";
import axios from "axios";
import { apiUrl } from "../../../setup/Api";

import { useNavigate } from "react-router-dom";
import { CheckAuth } from "../../../setup/AuthCheck";

export default function CustomerOrderAdmin() {
  const [resData, setResData] = useState({
    data: [],
    message: "",
  });

  const [message, setMessage] = useState("");

  const navigate = useNavigate();

  const getData = async () => {
    try {
      const response = await axios.get(
        apiUrl + "/api/customer_order/get_data",
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setResData({
        data: response.data.data,
        message: response.data.message,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const approveOrder = async (id) => {
    setMessage("");
    const confirmation = window.confirm("Are you sure approve this order ?");

    if (confirmation) {
      const response = await axios.post(
        apiUrl + "/api/customer_order/approve/" + id,
        {},
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setMessage(response.data.message);
      setResData("");
      getData();
    } else {
      return false;
    }
  };

  const declineOrder = async (id) => {
    setMessage("");
    const confirmation = window.confirm("Are you sure decline this order ?");

    if (confirmation) {
      const response = await axios.post(
        apiUrl + "/api/customer_order/decline/" + id,
        {},
        {
          headers: {
            Accept: "application/json",
            Authorization: "Bearer " + localStorage.getItem("Token"),
          },
        }
      );

      setMessage(response.data.message);
      setResData("");
      getData();
    } else {
      return false;
    }
  };

  const checkAuth = () => {
    const check = CheckAuth();

    if (check === false) {
      navigate("/");
    }
  };

  useEffect(() => {
    checkAuth();
    getData();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const columns = [
    {
      name: "Name",
      style: "font-bold",
      selector: (row) => row.get_customer.name,
      sortable: true,
    },
    {
      name: "Product",
      selector: (row) => row.get_product.name,
      sortable: true,
    },
    {
      name: "Order Date",
      cell: (row) => {
        var date = new Date(row.created_at);
        var newDate =
          date.getDate() +
          "-" +
          (date.getMonth() + 1) +
          "-" +
          date.getFullYear() +
          " " +
          date.getHours() +
          ":" +
          date.getMinutes() +
          ":" +
          date.getSeconds();

        return newDate;
      },
    },
    {
      name: "Status",
      cell: (row) => (
        <div>
          {row.status === 1 && (
            <h4 className='px-2 py-2 rounded bg-gray-600 font-bold text-white'>
              In Process
            </h4>
          )}
          {row.status === 2 && (
            <h4 className='px-2 py-2 rounded bg-green-600 font-bold text-white'>
              Approve
            </h4>
          )}
          {row.status === 3 && (
            <h4 className='px-2 py-2 rounded bg-red-600 font-bold text-white'>
              Reject
            </h4>
          )}
        </div>
      ),
      sortable: true,
    },

    {
      name: "Action",
      cell: (row) => (
        <div>
          {row.status === 1 && (
            <div>
              <button
                className='px-2 py-2 bg-red-600 text-white hover:bg-red-300 hover:text-black rounded mr-1'
                onClick={() => declineOrder(row.id)}>
                <i className='fa fa-times-circle' aria-hidden='true'></i>
              </button>
              <button
                className='px-2 py-2 bg-green-600 text-white hover:bg-green-300 hover:text-black rounded mr-1'
                onClick={() => approveOrder(row.id)}>
                <i className='fa fa-check-circle' aria-hidden='true'></i>
              </button>
            </div>
          )}
        </div>
      ),
    },
  ];

  const customStyles = {
    rows: {
      style: {
        minHeight: "72px",
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px", // override the cell padding for head cells
        paddingRight: "8px",
        fontWeight: "bold",
        fontSize: "150%",
      },
    },
    cells: {
      style: {
        paddingLeft: "8px", // override the cell padding for data cells
        paddingRight: "8px",
      },
    },
  };
  return (
    <div>
      <Navbar />
      <div className='pt-12 lg:flex'>
        <Aside />
        <div className='w-full h-full p-4 m-8 overflow-y-auto '>
          <h1 className='font-bold text-2xl mb-10 mt-10'>
            Menu Customer Order
          </h1>
          <div className='items-center p-16 mr-8'>
            <div className='mb-5 text-right'></div>
            {message && (
              <div
                className='flex p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800'
                role='alert'>
                <svg
                  className='inline flex-shrink-0 mr-3 w-5 h-5'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                    clip-rule='evenodd'></path>
                </svg>
                <div>
                  <span className='font-medium'>Success!</span> {message}
                </div>
              </div>
            )}
            <div className='border-4 border-b-cyan-700 h-full w-full shadow-lg'>
              <DataTable
                pagination
                columns={columns}
                data={resData.data}
                customStyles={customStyles}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
