export const apiUrl = "https://mifaabiyyu-crm-backend.herokuapp.com";

export const headersAuth = {
  Accept: "application/json",
  Authorization: "Bearer " + localStorage.getItem("Token"),
};

export const headersEditAuth = {
  "Content-Type": "application/x-www-form-urlencoded",
  Accept: "application/json",
  Authorization: "Bearer " + localStorage.getItem("Token"),
};
