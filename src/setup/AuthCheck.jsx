// import { useNavigate } from "react-router-dom";

export const CheckAuth = () => {
  //   const navigate = useNavigate();
  const token = localStorage.getItem("Token");
  const user = localStorage.getItem("User");

  const userData = JSON.parse(user);
  if (!token) {
    return false;
  }

  if (userData) {
    if (userData.roles !== "1") {
      return false;
    }
  }

  return true;
};

export const CheckAuthCustomer = () => {
  //   const navigate = useNavigate();
  const token = localStorage.getItem("Token");
  const user = localStorage.getItem("User");

  const userData = JSON.parse(user);
  if (!token) {
    return false;
  }

  if (!userData) {
    return false;
  }

  return true;
};
