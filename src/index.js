import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "./layout/auth/Register";
import { Login } from "./layout/auth/Login";
import Dashboard from "./layout/admin/Dashboard";
import Customer from "./layout/admin/customer/Customer";
import AddCustomer from "./layout/admin/customer/AddCustomer";
import EditCustomer from "./layout/admin/customer/EditCustomer";
import Products from "./layout/admin/products/Products";
import AddProduct from "./layout/admin/products/AddProduct";
import EditProduct from "./layout/admin/products/EditProduct";
import Users from "./layout/admin/users/Users";
import AddUser from "./layout/admin/users/AddUser";
import EditUser from "./layout/admin/users/EditUser";
import ProductsAll from "./layout/landing/Products";
import CheckOut from "./layout/landing/CheckOut";
import CustomerOrder from "./layout/landing/CustomerOrder";
import CustomerOrderAdmin from "./layout/admin/customerOrder/CustomerOrder";
import Category from "./layout/admin/categoryAndCoupon/Category";
import Coupon from "./layout/admin/categoryAndCoupon/Coupon";
import AddCoupon from "./layout/admin/categoryAndCoupon/AddCoupon";
import EditCoupon from "./layout/admin/categoryAndCoupon/EditCoupon";
import "flowbite";
import Message from "./layout/admin/Message";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />} />
        <Route path='/all-product' element={<ProductsAll />} />
        <Route path='/checkout-product/:slug' element={<CheckOut />} />
        <Route path='/customer-order-product' element={<CustomerOrder />} />

        <Route path='/register' element={<Register />} />
        <Route path='/login' element={<Login />} />
        <Route path='/dashboard' element={<Dashboard />} />

        {/* Customer */}
        <Route path='/customer' element={<Customer />} />
        <Route path='/customer/add' element={<AddCustomer />} />
        <Route path='/customer/edit/:id' element={<EditCustomer />} />

        {/* Products */}
        <Route path='/product' element={<Products />} />
        <Route path='/product/add' element={<AddProduct />} />
        <Route path='/product/edit/:id' element={<EditProduct />} />

        {/* Users */}
        <Route path='/user' element={<Users />} />
        <Route path='/user/add' element={<AddUser />} />
        <Route path='/user/edit/:id' element={<EditUser />} />

        {/* Customer Order */}
        <Route path='/customer-order' element={<CustomerOrderAdmin />} />
        <Route path='/customer-order/add' element={<AddUser />} />
        <Route path='/customer-order/:id' element={<EditUser />} />

        {/* Category And Coupon */}
        <Route path='/category' element={<Category />} />
        <Route path='/coupon' element={<Coupon />} />
        <Route path='/coupon/add' element={<AddCoupon />} />
        <Route path='/coupon/edit/:id' element={<EditCoupon />} />

        {/* Message */}
        <Route path='/message' element={<Message />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
